require 'test_helper'

class FixedReservesControllerTest < ActionController::TestCase
  setup do
    @fixed_reserf = fixed_reserves(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fixed_reserves)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fixed_reserf" do
    assert_difference('FixedReserve.count') do
      post :create, fixed_reserf: { schedule: @fixed_reserf.schedule, week_day: @fixed_reserf.week_day }
    end

    assert_redirected_to fixed_reserf_path(assigns(:fixed_reserf))
  end

  test "should show fixed_reserf" do
    get :show, id: @fixed_reserf
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fixed_reserf
    assert_response :success
  end

  test "should update fixed_reserf" do
    put :update, id: @fixed_reserf, fixed_reserf: { schedule: @fixed_reserf.schedule, week_day: @fixed_reserf.week_day }
    assert_redirected_to fixed_reserf_path(assigns(:fixed_reserf))
  end

  test "should destroy fixed_reserf" do
    assert_difference('FixedReserve.count', -1) do
      delete :destroy, id: @fixed_reserf
    end

    assert_redirected_to fixed_reserves_path
  end
end
