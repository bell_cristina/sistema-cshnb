class Address < ActiveRecord::Base
  belongs_to :user

  attr_accessible :city,
                  :complement,
                  :country,
                  :district,
                  :numberzip_code,
                  :state,
                  :street,
                  :user_id
end
