#encoding: utf-8
class Travel < ActiveRecord::Base
  belongs_to :request
  belongs_to :vehicle

  has_many :travel_expenses, :dependent => :destroy
  has_many :travel_drivers, :dependent => :destroy
  has_many :monthlies, :dependent => :destroy


  attr_accessible :request_id, :vehicle_id, :travel_expenses_attributes,
  				  :travel_drivers_attributes, :status,:monthlies_attributes, :deleted


  validates :request_id, presence: true
  validates :vehicle_id, presence: true
  validates :valida_veiculo, acceptance: {accept: true, message: 'veículo não disponível'}

  
 
 def valida_veiculo
    requestes = Request.where(deleted: false)
    requestes = requestes.select {|r| r.travel != nil }
    requestes = requestes.reject {|request| self.request_id == request.id }
    requestes = requestes.select{|request| self.request.date_of_departure.between?(request.date_of_departure, request.return_date) || self.request.return_date.between?(request.date_of_departure, request.return_date)}

    if requestes.empty? 
      return true
    else
      requestes = requestes.select {|request| self.vehicle_id == request.travel.vehicle_id}
      if requestes.empty?
        return true
      else
        return false
      end
    end
   
 end
end
