class Duration < ActiveRecord::Base
  belongs_to :user
  belongs_to :function
  attr_accessible :acting, :end, :initiation, :user_id, :function_id
end
