class TravelExpense < ActiveRecord::Base
  belongs_to :travel



  attr_accessible :justification, :price, :travel_id, :deleted

  validates :price, presence: true
  validates :justification, presence: true
  validates :travel_id, presence: true




end
