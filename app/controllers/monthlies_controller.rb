class MonthliesController < ApplicationController
  
  def index
    @monthlies = Monthly.all
  end

  def show
    @monthly = Monthly.find(params[:id])
  end

  def new
    @monthly = Monthly.new
  end

  def edit
    @monthly = Monthly.find(params[:id])
  end

  def create
    @monthly = Monthly.new(params[:monthly])

    if @monthly.save
      redirect_to monthlies_path
    else
      render :new
    end
  end

  def update
    @monthly = Monthly.find(params[:id])

    if @monthly.update_attributes(params[:monthly])
      redirect_to monthlies_path
    else
      render :edit
    end
  end

  def destroy
    @monthly = Monthly.find(params[:id])
    @monthly.update_attribute(:deleted, true)
    redirect_to monthlies_path
  end
end
