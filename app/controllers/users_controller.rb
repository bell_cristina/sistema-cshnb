#encoding: utf-8
class UsersController < ApplicationController

  include UsersHelper
  include AdministratorsHelper

  # before_filter :usuario_logado, only: [:login]

  # before_filter only: [:new, :create] do
  #   redirect_to notfound_path unless upper_lvl_3?
  # end

  def index
    @users = User.where(deleted: false)
  end

  def login
    @user = User.new
  end

  def logout
    end_session_user
    redirect_to user_login_path
  end

  def authentication
    @user = User.new(params[:user])
    if authenticate_user(@user)
      redirect_to home_path
    else
      render :login
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def new 
    if administrator_authenticated? || (user_authenticated? && current_user.durations.last.function.level>=3)
      @pwd = code_generate
      @user = User.new
    else 
      redirect_to notfound_path
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def create
    if administrator_authenticated? || (user_authenticated? && current_user.durations.last.function.level>=3)
      @user = User.new(params[:user])
      @user.deleted = false
    else 
      redirect_to notfound_path
    end

    if @user.save
      redirect_to users_path
    else
      render :new
    end
  end

  def update
    @user = User.find(params[:id])

    if @user.update_attributes(params[:user])
      redirect_to users_path
    else
      render :edit
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.update_attributes(deleted: true)
    redirect_to users_path
  end

  def edit_profile
    @user = User.find(session[:user].id)
  end

  def update_profile
    @user = User.find(session[:user].id)
    if @user.update_attributes(params[:user])
      redirect_to root_path
    else
      render :edit_profile
    end
  end

  def edit_password
    @user = User.new
  end

  def update_password
    @user = User.new(params[:user])
    if change_password(@user.current_password, @user.password, @user.password_confirmation)
      redirect_to root_path
    else
      render :edit_password
    end
  end
end
