class DriversController < ApplicationController
  
  include UsersHelper

  # before_filter :autorizacao_chefe_setor_transporte, except: []

  def index
    @drivers = Driver.where(deleted: false)
  end

  def show
    @driver = Driver.find(params[:id])
  end

  def new
    @driver = Driver.new
  end

  def edit
    @driver = Driver.find(params[:id])
  end

  def create
    @driver = Driver.new(params[:driver])
    @driver.deleted = false
 
    if @driver.save
      redirect_to drivers_path
    else
      render :new
    end
  end

  def update
    @driver = Driver.find(params[:id])
    
    if @driver.update_attributes(params[:driver])
      redirect_to drivers_path
    else
      render :edit
    end
  end

  def destroy
    @driver = Driver.find(params[:id])
    @driver.update_attribute(:deleted, true)
    redirect_to drivers_path
  end
end
