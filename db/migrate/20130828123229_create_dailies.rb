class CreateDailies < ActiveRecord::Migration
  def change
    create_table :dailies do |t|
      t.float :daily

      t.timestamps
    end
  end
end
