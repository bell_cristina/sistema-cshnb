class CreateHolidays < ActiveRecord::Migration
  def change
    create_table :holidays do |t|
      t.string :description
      t.integer :day
      t.integer :month
      t.boolean :fixed
      t.date :date
      t.boolean :deleted

      t.timestamps
    end
  end
end
