class CreateParticipants < ActiveRecord::Migration
  def change
    create_table :participants do |t|
      t.references :request
      t.references :user
      t.boolean :deleted

      t.timestamps
    end
    add_index :participants, :request_id
    add_index :participants, :user_id
  end
end
