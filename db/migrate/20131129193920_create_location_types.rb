class CreateLocationTypes < ActiveRecord::Migration
  def change
    create_table :location_types do |t|
      t.string :name
      t.boolean :deleted

      t.timestamps
    end
  end
end
