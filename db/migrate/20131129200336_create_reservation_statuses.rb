class CreateReservationStatuses < ActiveRecord::Migration
  def change
    create_table :reservation_statuses do |t|
      t.string :name
      t.boolean :deleted

      t.timestamps
    end
  end
end
