class CreateDurations < ActiveRecord::Migration
  def change
    create_table :durations do |t|
      t.date :initiation
      t.date :end
      t.boolean :acting
      t.references :user
      t.references :function

      t.timestamps
    end
    add_index :durations, :user_id
    add_index :durations, :function_id
  end
end
