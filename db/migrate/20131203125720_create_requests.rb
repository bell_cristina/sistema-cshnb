class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.date :date
      t.date :date_of_departure
      t.date :return_date
      t.text :goal
      t.text :justification
      t.string :destination
      t.integer :distance
      t.boolean :deleted
      t.references :user

      t.timestamps
    end
    add_index :requests, :user_id
  end
end
